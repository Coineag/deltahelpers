"""
Module with operations to help evaluate single steps of delta.

@author: Beate Gericke
"""

import sys

sys.path.append('../delta-dev')
import bioformats
import delta.utilities as utils
from delta.data import saveResult_seg, predictGenerator_seg, postprocess, readreshape, seg_weights_2D
from delta.model import unet_seg
from delta.utilities import cfg
import os
import glob
import numpy as np
from pathlib import Path
import skimage.io as io
import cv2

"""
creating the path to save files to, depending on input file/folder from config file

Parameters
----------
save_location : Path
    Path to folder/ file from config
----------
Returns
save_location : Path
Path to save the output data to

"""


def output_path(save_location):
    # checking if the given path is a file or a folder
    # if it is a file (like .czi, create a folder to save files to

    if type(save_location) != 'Path':
        save_location = Path(save_location)

    if save_location.is_dir():
        print('This is not a czi-file to split.')
    else:
        save_location = save_location.with_name(save_location.stem)
    if not os.path.exists:
        save_location.mkdir()
    return save_location

"""
Checking if the given input from the config is a file or a folder. 
If it is a file checking if the folder from the czi-split function is there. 
If it is, set it as input, if not, give a warning.

Parameters
----------
input_path : Path
    Path to folder/ file from config
----------
Returns
inputs_path : String
inputs_path
"""
def get_inputs_folder(inputs_path):
    inputs_path = Path(inputs_path)
    if not inputs_path.is_dir():
        inputs_path = inputs_path.with_name(inputs_path.stem)

    if not inputs_path.exists():
        print('The folder you want to access does not exist. Please check if you need to run a czi-split first.')
    return str(inputs_path)

"""
splitting a czi file and saving the timelapse in single tif files

Parameters
----------
input_path : string
    filepath to czi file as string   
----------
Returns
None

"""
def czi_split(input_path):
    # getting meta data from the image by using the xpreader
    xpreader = utils.xpreader(input_path, use_bioformats=True)
    # creating filehandle to handle czi file
    filehandle = bioformats.ImageReader(input_path)
    meta = bioformats.get_omexml_metadata(input_path)
    md = bioformats.OMEXML(meta)

    positions = [p for p in range(xpreader.positions)]
    channels = [c for c in range(xpreader.channels)]
    timepoints = [f for f in range(xpreader.timepoints)]
    # getting output path to save files to
    tif_path = output_path(xpreader.filename)

    # splitting the file and save tifs
    for p, pos in enumerate(positions):
        for c, cha in enumerate(channels):
            # check for Channel Name to only save phase contrast channel
            channelname = md.image().Pixels.Channel(c).Name
            if channelname != 'Phase':
                continue
            for t, tp in enumerate(timepoints):
                # creating filename to save tif
                filename_tif = 'pos{position}cha{channel}fra{frame:06d}'.format(position=p, channel=c, frame=t)
                file = os.path.join(tif_path, filename_tif + '.tif')
                frame = filehandle.read(series=p, c=c, t=t, rescale=False)
                io.imsave(file, frame, plugin='tifffile', append=True)
    print('Tif files saved.')

"""
saving masks for a time lapse, given in single images.

Parameters
----------
None
----------
Returns
None

@author: jblugagne

"""
def save_masks():

    # Inputs folder:
    inputs_folder = get_inputs_folder(cfg.eval_movie)
    if inputs_folder == '':
        print('There is no input folder defined. Please check, if you loaded a config.')

    # Outputs folder:
    outputs_folder = os.path.join(output_path(Path(inputs_folder)), "../predictions")
    if not os.path.exists(outputs_folder):
        os.makedirs(outputs_folder)

    # Get ablsolute path, elsewise delta/data.py readreshape()-cv2.imread() can't find file
    outputs_folder = os.path.abspath(outputs_folder)
    inputs_folder = os.path.abspath(inputs_folder)

    # List files in inputs folder:
    unprocessed = sorted(
        glob.glob(inputs_folder + "/*.tif") + glob.glob(inputs_folder + "/*.png")
    )

    # Load up model:
    model = unet_seg(input_size=cfg.target_size_seg + (1,))
    model.load_weights(cfg.model_file_seg)

    # Process
    while unprocessed:
        # Pop out filenames
        ps = min(4096, len(unprocessed))  # 4096 at a time
        to_process = unprocessed[0:ps]
        del unprocessed[0:ps]

        # Input data generator:
        predGene = predictGenerator_seg(
            inputs_folder,
            files_list=to_process,
            target_size=cfg.target_size_seg,
            crop=cfg.crop_windows,
        )

        # mother machine: Don't crop images into windows
        if not cfg.crop_windows:
            # Predictions:
            results = model.predict(predGene, verbose=1)[:, :, :, 0]

        # 2D: Cut into overlapping windows
        else:
            img = readreshape(
                os.path.join(inputs_folder, to_process[0]),
                target_size=cfg.target_size_seg,
                crop=True,
            )
            # Create array to store predictions
            results = np.zeros((len(to_process), img.shape[0], img.shape[1], 1))
            # Crop, segment, stitch and store predictions in results
            for i in range(len(to_process)):
                # Crop each frame into overlapping windows:
                windows, loc_y, loc_x = utils.create_windows(
                    next(predGene)[0, :, :], target_size=cfg.target_size_seg
                )
                # We have to play around with tensor dimensions to conform to
                # tensorflow's functions:
                windows = windows[:, :, :, np.newaxis]
                # Predictions:
                pred = model.predict(windows, verbose=1, steps=windows.shape[0])
                # Stich prediction frames back together:
                pred = utils.stitch_pic(pred[:, :, :, 0], loc_y, loc_x)
                pred = pred[np.newaxis, :, :, np.newaxis]  # Mess around with dims

                results[i] = pred

        # Post process results (binarize + light morphology-based cleaning):
        results = postprocess(results, crop=cfg.crop_windows)

        # Save to disk:
        saveResult_seg(outputs_folder, results, files_list=to_process)

"""
saving weight maps for a new training data set for segmentation.

Parameters
----------
None
----------
Returns
None

"""
def save_seg_weightmaps():
    seg_folder = os.path.abspath(os.path.join(get_inputs_folder(cfg.training_set_seg), 'seg'))
    wei_folder = os.path.abspath(os.path.join(get_inputs_folder(cfg.training_set_seg), 'wei'))
    images = sorted(
        glob.glob(seg_folder + "/*.tif")
    )
    for image in images:
        image = os.path.basename(image)
        img = cv2.imread(os.path.join(seg_folder, image))
        imgarray = np.array(img)
        wei = seg_weights_2D(imgarray)
        # normalizing the values to uint8
        n_wei = cv2.normalize(wei, None, 0, 255, cv2.NORM_MINMAX, cv2.CV_8U)
        cv2.imwrite(os.path.join(wei_folder, image), n_wei)


"""
converting .png or .tiff to .tif

Parameters
----------
string: folder
----------
Returns
None

"""
def png_to_tif(folder):
    folder = Path(folder)
    folder = os.path.abspath(folder)
    images = sorted(
        glob.glob(folder + "/*.tiff") + glob.glob(folder + "/*.png")
    )
    for image in images:
        image_name = Path(image).stem
        img = cv2.imread(image)
        cv2.imwrite(os.path.join(folder, image_name + '.tif'), img)
        os.remove(image)

