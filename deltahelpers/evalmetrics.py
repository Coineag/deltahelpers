"""
Module with functions to calculate metrics

@author: Beate Gericke
"""

import sys
sys.path.append('../delta-dev')

from pathlib import Path
import sklearn.metrics as metrics
import numpy as np
import matplotlib.pylab as plt
import cv2
import os
import glob
import pandas as pd

"""
Calculating metrics

Parameters
----------
save_location : Path
    Path to folder/ file from config
----------
Returns
df : dataframe with the metrics
Path to save the output data to

"""
def calculate_metrics(config_path):
    config_path = Path(config_path)
    # getting ground truth and predictions path DeLTA test set
    gt_path = os.path.abspath(os.path.join(config_path, './../seg'))
    pred_deltamodel = os.path.abspath(os.path.join(gt_path, './../predictions/predicted_with_delta_train'))
    pred_mpimodel = os.path.abspath(os.path.join(gt_path, './../predictions/predicted_with_mpi_train'))

    files = [os.path.basename(x) for x in glob.glob(gt_path + "/*.tif")]
    df = pd.DataFrame(
        columns=['Accuracy prediction with DeLTA', 'Accuracy prediction with MPI', 'IoU prediction with DeLTA',
                 'IoU prediction with MPI'], index=files)

    for filename in files:
        gt = os.path.join(gt_path, filename)
        pred_delta = os.path.join(pred_deltamodel, filename)
        pred_mpi = os.path.join(pred_mpimodel, filename)
        groundtruth = cv2.imread(gt, cv2.IMREAD_GRAYSCALE)
        p_delta = cv2.imread(pred_delta, cv2.IMREAD_GRAYSCALE)
        p_mpi = cv2.imread(pred_mpi, cv2.IMREAD_GRAYSCALE)

        acc_delta = metrics.accuracy_score(groundtruth, p_delta)
        acc_mpi = metrics.accuracy_score(groundtruth, p_mpi)
        jac_delta = metrics.jaccard_score(groundtruth, p_delta, average='weighted')
        jac_mpi = metrics.jaccard_score(groundtruth, p_mpi, average='weighted')
        df.loc[filename] = pd.Series(
            {'Accuracy prediction with DeLTA': acc_delta, 'Accuracy prediction with MPI': acc_mpi,
             'IoU prediction with DeLTA': jac_delta, 'IoU prediction with MPI': jac_mpi})

    return df